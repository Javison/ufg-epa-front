UFirstgroup - Frontend Assesment
===============

This repository contains code for **UFirstgroup assesment task 2** witch requires:<br>
> Create one or more HTML- and Javascript-Files that read the JSON-File and render the following analysis > graphically as charts:
> 
> - Requests per minute over the entire time span
> - Distribution of HTTP methods (GET, POST, HEAD,...)
> - Distribution of HTTP answer codes (200, 404, 302,...)
> - Distribution of the size of the answer of all requests with code 200 and size < 1000B

<br><br>


Table of Contents
---------------

- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
- [Run locally](#run-locally)
- [Deploy](#deploy)
- [License](#license)

<br><br>


Getting Started
---------------


### Requirements

- Node.js [link](https://nodejs.org/en/)
- npm (With Node)
- git version control system [link](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- Angular framwork [link](https://angular.io/guide/setup-local)
- firebase-tools [link](https://firebase.google.com/docs/web/setup)

<br>

### Clone repository & install

Download the code:

```bash
# Get the latest snapshot
git clone --depth=1 https://Javison@bitbucket.org/Javison/ufg-epa-front.git ufg-epa-front

# Change directory
cd ufg-epa-front

# Install NPM dependencies
npm install
```

Install toolkit and access your account if you are going to use backend services:

```bash
$ npm install -g firebase-tools
$ firebase login
# You must receive project access after login
$ firebase projects:list
$ firebase use ufg-epa
```


#### Proxy settings reminder

Add proxy:

```bash
# npm
npm config set proxy http://userproxy:passwordproxy@proxy.company.es:8080
npm config set https-proxy http://userproxy:passwordproxy@proxy.company.es:8080
# git
git config --global http.proxy http://userproxy:passwordproxy@proxy.company.es:8080
git config --global https.proxy https://userproxy:passwordproxy@proxy.company.es:8080
```

Remove proxy:

```bash
# npm
npm config rm proxy
npm config rm https-proxy
# git
git config --global --unset http.proxy
git config --global --get http.proxy https://userproxy:passwordproxy@proxy.company.es:8080
```

<br><br>


Project Structure
---------------

| Name                               | Description                                                  |
| ---------------------------------- | ------------------------------------------------------------ |
| **src/app**                        | Root code folder                                             |
| **src/assets**                     | Images and other static files                                |
| **src/environments**               | Environment configuration files: local, stage, prod          |
| **src/app/pages**                  | Main project pages                                           |
| **src/app/shared**                 | Shared code among the project                                |
| **src/app/shared/components**      | Reusable page sections such as footer, navbar, sidebar..     |
| **src/app/shared/guards**          | Restrict access to certain parts of website.                 |
| **src/app/shared/interfaces**      | Data model descriptions                                      |
| **src/app/shared/models**          | Data classes to implement                                    |
| **src/app/shared/redux**           | Redux state management pattern available over the project    |
| **src/app/shared/resolvers**       | Return promiso or observer with condition before load a path |
| **src/app/shared/services**        | Main business logic. Class with well-defined purpose.        |
| **src/app/shared/utils**           | Static methods for different purpose.                        |
| **dist**/                          | Public folder with build (tsc) components to deploy          |
| **coverage**/                      | Testing coverage html reports                                |
| **documentation**/                 | Project technical documentation                              |
| **node_modules**/                  | npm dependency packages                                      |
| .gitignore                         | Git ignore config.                                           |
| package.json                       | NPM dependencies.                                            |
| firebase.json                      | Firebase configuration file                                  |
| angular.json                       | Angular configuration file                                   |
| tsconfig.json                      | Typescript configuration file                                |
| tslint.json                        | TypeScript analysis tool that checks code for readability    |

<br><br>


Run locally
---------------

```bash
# If you are running backend server (Database and functions) # or ng serve --c=local
$ npm run start:local 

# If you are NOT running backend server locally, 
# you can connect to cloud environment (Since this assignment lacks of staging server, be aware that you will connect to production...)
$ npm run start:stage
```

<br><br>


Deploy
---------------

```bash
# Firebase login (Must have access to project)
$ firebase login

# Select the project
$ firebase projects:list
$ firebase use ufg-epa

# Build
$ npm run build:release

# Deploy
$ npm run deploy:release
```

<br><br>


Coding recommendations
---------------

### Integrated development environment 
App developed with Visual studio code [link](https://code.visualstudio.com/).<br>
Plugins used for Visual Studio Code:
- Angular Language Service
- angular2-inline
- Bootstrap 4, Font awesome 4
- Bracket Pair Colorizer 2
- GitLens
- jshint
- Prettier
- TypeScript Hero
- TypeScript Importer
- vscode-icons

<br>

### Anonymous Authentication

We applied [anonymous authentication](https://firebase.google.com/docs/auth/?authuser=0)
to protect the project from abuse, we limit the number of Anonymous sign-ups that the application can have from the same IP address. Current quota is 100 per hour.
You can request and schedule changes to this quota by asking the project administrator.

<br>

### Redux pattern

App state managed by redux pattern architecture.
Single source of truth, State is read-only, Changes are made with pure functions
More at [redux.js.org](https://redux.js.org/)

<br>

### Standarts
- Write comments and documentation
- Write readable yet efficient code
- Write tests
- SOLID principles (more info)[https://en.wikipedia.org/wiki/SOLID]


<br><br>


License
---------------

Copyright (c) 2020 Javier Gracia Muro
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UploadLogFileComponent } from './pages/upload-log-file/upload-log-file.component';
import { GraphicalAnalysisComponent } from './pages/graphical-analysis/graphical-analysis.component';
import { AuthAnonymousService } from './shared/resolvers/auth-anonymous.resolver';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [
  
  { path: '', pathMatch: 'full',
    component: HomeComponent
  },
  { path: 'graphical-analysis', 
    component: GraphicalAnalysisComponent, 
    resolve: {
      authAnonymousResolver: AuthAnonymousService,
    }
  },
  { path: 'file-processing', 
    component: UploadLogFileComponent, 
    resolve: {
      authAnonymousResolver: AuthAnonymousService,
    }
  },
  
  // Redirect 404 page or home
  { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  providers: [AuthAnonymousService],
  exports: [RouterModule]
})
export class AppRoutingModule { }

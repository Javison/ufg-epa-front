import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { GraphicalAnalysisComponent } from './graphical-analysis.component';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { AngularFireDatabase } from '@angular/fire/database';
import { Store } from '@ngrx/store';
import { AppState } from '../../shared/redux/app.reducers';

describe('GraphicalAnalysisComponent', () => {
  let component: GraphicalAnalysisComponent;
  let fixture: ComponentFixture<GraphicalAnalysisComponent>;
 
  beforeEach( () => {
    let store: MockStore;
    const initialState = {
      "showSidebar": false,
      "loadingItems": [
        "log_file_metadata"
      ]
    };

    TestBed.configureTestingModule({
      declarations: [ GraphicalAnalysisComponent ],
      imports: [ LoggerTestingModule ],
      providers: [
        { provide: AngularFireDatabase, useValue: ''},
        provideMockStore({ initialState })
      ]
    });

    store = TestBed.inject(MockStore);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphicalAnalysisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterEach(() => {
    fixture.destroy();
 });
});

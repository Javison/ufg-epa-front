import { Component, OnInit, OnDestroy, AfterViewInit, HostListener } from '@angular/core';
import { Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { NGXLogger } from 'ngx-logger';
import { AppState } from '../../shared/redux/app.reducers';
import { ChartAnalysisService } from '../../shared/services/chart-analysis.service';

/**
 * Load following charts:
 * Requests per minute over the entire time span
 * Distribution of HTTP methods (GET, POST, HEAD,...)
 * Distribution of HTTP answer codes (200, 404, 302,...)
 * Distribution of the size of the answer of all requests with code 200 and size < 1000B
 */
@Component({
  selector: 'app-graphical-analysis',
  templateUrl: './graphical-analysis.component.html',
  styleUrls: ['./graphical-analysis.component.css']
})
export class GraphicalAnalysisComponent implements OnInit, AfterViewInit, OnDestroy {

  isLoading: boolean = true;
  isLoadingView: boolean = true;
  legendPosition: string = 'buttom';
  uiSubscription: Subscription = new Subscription();

  requestMethodsChartData: any[] = [];
  requestMethodsChartDimensions: any[] = [700, 300];  
  requestsMethodsChartColorScheme = { 
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA'] 
  };

  responseCodesChartData: any[] = [];
  responseCodesChartDimensions: any[] = [700, 400];
  responseCodesChartColorScheme = { 
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5'] 
  };
  
  responseSizeChartData: any[] = [];
  responseSizeChartDimensions: any[] = [700, 400];
  responseSizeChartxAxisLabel: string = 'Response size on range of 10 Bytes';
  responseSizeChartyAxisLabel: string = '# Times Delivered';
  responseSizeChartColorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };

  requestsPerMinuteChartData: any[] = []
  requestsPerMinuteChartDimensions: any[] = [700, 400];  
  requestsPerMinuteChartxAxisLabel: string = 'Datetime';
  requestsPerMinuteChartyAxisLabel: string = '# Requests per minute';
  requestsPerMinuteChartColorScheme = {
    domain: ['#5AA454', '#E44D25', '#CFC0BB', '#7aa3e5', '#a8385d', '#aae3f5']
  };


  constructor(private log: NGXLogger,
              private store: Store<AppState>,
              private chartAnalysisService: ChartAnalysisService) { }
    
  async ngOnInit() {

    this.uiSubscription = this.store.select('ui')
      .subscribe( ui => {
        this.log.debug('GraphicalAnalysisComponent > ngOnInit - ui.loadingItems: ', ui.loadingItems);
        this.isLoading = ui.loadingItems.length > 0;
    });

    await this.chartAnalysisService.getChartData();

    this.getHttpRequestMethodsChartData();
    this.getHttpResponseCodesChartData();
    this.getRequestPerMinuteChartData();
    this.getResponsesSizeData();

    this.isLoadingView = false;
  }
  
  ngOnDestroy() {    
    this.uiSubscription.unsubscribe();
  }

  ngAfterViewInit() {
    this.log.debug('GraphicalAnalysisComponent > ngAfterViewInit');
    this.setGraphsDimensions();
  }

  /**
   * Triggered on screen resize
   * @param event window resize event
   */
  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.log.debug('GraphicalAnalysisComponent > onResize');
    this.setGraphsDimensions();
  }

  /**
   * Setup chart dimensions according to screen size
   */
  setGraphsDimensions() {
    this.log.debug('GraphicalAnalysisComponent > setGraphsDimensions - window size:',  window.innerWidth, window.innerHeight);
    const chartWidth = window.innerWidth * 0.95;
    const chartHeight = window.innerHeight * 0.5;
    this.requestsPerMinuteChartDimensions = [chartWidth, chartHeight];
    this.requestMethodsChartDimensions = [chartWidth, chartHeight];
    this.responseCodesChartDimensions = [chartWidth, chartHeight];
    this.responseSizeChartDimensions = [chartWidth, chartHeight];
    this.log.debug('GraphicalAnalysisComponent > setGraphsDimensions - chartWidth, chartHeight:', chartWidth, chartHeight);
  }

  /**
   * Requests per minute over the entire time span
   */
  getRequestPerMinuteChartData() {
    let lineChartRequestsPerMinuteEntries: {name: string, value: number}[] = [];

    this.chartAnalysisService.httpRequestsPerMinute.forEach( (value: number, key: string) => {
      lineChartRequestsPerMinuteEntries.push({ name: key, value: value });
    });
    // [ { name: 'Legend label', series: [ {name: string, value: number}, ...] }, ... ];
    this.requestsPerMinuteChartData = [ { name: 'Requests per minute', series: lineChartRequestsPerMinuteEntries } ];
  }
  
  /**
   * Distribution of HTTP methods (GET, POST, HEAD,...)
   */
  getHttpRequestMethodsChartData() {
    this.log.debug('GraphicalAnalysisComponent > getHttpRequestMethodsChartData');
    this.chartAnalysisService.httpRequestMethods.forEach( (value, key) => {
      this.requestMethodsChartData.push({ name: key, value: value});
    }); 
  }

  /**
   * Distribution of HTTP answer codes (200, 404, 302,...)
   */
  getHttpResponseCodesChartData() {
    this.log.debug('GraphicalAnalysisComponent > getHttpResponseCodesChartData');
    this.chartAnalysisService.httpResponseCodes.forEach( (value, key) => {
      this.responseCodesChartData.push({ name: key, value: value});
    }); 
  }

  /**
   * Distribution of the size of the answer of all requests with code 200 and size < 1000B
   */
  getResponsesSizeData() {
    let responseSizeEntries: {name: string, value: number}[] = [];

    this.chartAnalysisService.httpResponseSizes.forEach( (value: number, key: number) => {
      responseSizeEntries.push({ name: `${key * 10}`, value: value });
    });
    // [ { name: 'Legend label', series: [ {name: string, value: number}, ...] }, ... ];
    this.responseSizeChartData = [ { name: 'Response Size', series: responseSizeEntries } ];
  }

}
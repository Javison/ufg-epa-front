import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { UploadLogFileComponent } from './upload-log-file.component';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireDatabase } from '@angular/fire/database';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

describe('UploadLogFileComponent', () => {
  let component: UploadLogFileComponent;
  let fixture: ComponentFixture<UploadLogFileComponent>;

  beforeEach(async(() => {
    
    let store: MockStore;
    const initialState = {
      "showSidebar": false,
      "loadingItems": [
        "log_file_metadata"
      ]
    };

    TestBed.configureTestingModule({
      declarations: [ UploadLogFileComponent ],
      imports: [
        LoggerTestingModule,
        BrowserAnimationsModule, 
        ToastrModule.forRoot()
      ],
      providers: [
        { provide: AngularFireStorage, useValue: '' },
        { provide: AngularFireDatabase, useValue: '' },
        provideMockStore({ initialState })
      ]
    });

    store = TestBed.inject(MockStore);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadLogFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  afterEach(() => {
    fixture.destroy();
  });

});

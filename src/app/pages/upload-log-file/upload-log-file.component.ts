import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize, filter } from 'rxjs/operators';
import { LogFileTxtMetadata } from '../../shared/models/log-file-txt-metadata.model';
import { UploadLogFileService } from '../../shared/services/upload-log-file.service';
import { UIService } from '../../shared/services/ui.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../shared/redux/app.reducers';
import { LOAD_ITEM_UPLOADING_FILE } from '../../shared/redux/actions/ui.actions';

@Component({
  selector: 'app-upload-log-file',
  templateUrl: './upload-log-file.component.html',
  styleUrls: ['./upload-log-file.component.css']
})
export class UploadLogFileComponent implements OnInit, OnDestroy {

  readonly STORAGE_PATH: string = '/server_analysis/source_files/';
  readonly PREFIX_UPLOADED_FILE: string = 'source_';

  isLoading: boolean = false;
  isUploadingFile: boolean = false;
  uploadFileProgress: number = 0;
  logFileTxtMetadata: LogFileTxtMetadata;

  uiSubscription: Subscription = new Subscription();
  uploadFileProgressSubscription: Subscription = new Subscription();
  logFileMetadataSubscription: Subscription = new Subscription();

  constructor(private log: NGXLogger,
              private store: Store<AppState>,
              public uploadLogFileService: UploadLogFileService) { }
  
  ngOnInit() {

    this.uiSubscription = this.store.select('ui')
      .subscribe( ui => {
        this.log.debug('UploadLogFileComponent > ngOnInit - ui.loadingItems: ', ui.loadingItems);
        this.isLoading = ui.loadingItems.length > 0;
        this.isUploadingFile = ui.loadingItems.indexOf(LOAD_ITEM_UPLOADING_FILE) !== -1;
    });

    this.subscribeReduxLogFileMetadata();

    const previousUploadId = localStorage.getItem('logFileTxtMetadataId')
    if (previousUploadId) {
      this.uploadLogFileService.subscribeLogFileMetadata(previousUploadId);
    }
    
  }

  ngOnDestroy() {
    this.uiSubscription.unsubscribe();
    this.logFileMetadataSubscription.unsubscribe();
    this.uploadFileProgressSubscription.unsubscribe();
    this.uploadLogFileService.cancellSubscriptions();
  }

  /**
   * Upload txt file to /server_analysis/source_files/
   * 
   */
  onClickUploadFile(event) {
    this.log.debug('UploadLogFileComponent > onClickUploadFile');

    this.clearPreviousUploadedData();
    this.uploadLogFileService.uploadFile(event.target.files[0], this.PREFIX_UPLOADED_FILE, this.STORAGE_PATH);   

    this.uploadFileProgressSubscription =
      this.uploadLogFileService.uploadProgress$
        .subscribe( (progress) => {
          this.uploadFileProgress = progress;
      });
  }
  
  /**
   * Subscribe to backend information about the uploaded file
   */
  subscribeReduxLogFileMetadata() {
    this.logFileMetadataSubscription = this.store.select('logFileMetadata')
      .pipe(
        filter( (logFileMetadataState) => 
          logFileMetadataState.logFileMetadata != null 
          && JSON.stringify(logFileMetadataState.logFileMetadata) !== '{}'
        )
      )
      .subscribe( (logFileMetadataState) => {
        this.log.debug('UploadLogFileComponent > subscribeReduxLogFileMetadata - logFileMetadataState.logFileMetadata: ', logFileMetadataState.logFileMetadata);
        this.logFileTxtMetadata = logFileMetadataState.logFileMetadata;
    });
  }

  /**
   * When user attemp to upload a second file we clear shown info
   */
  clearPreviousUploadedData() {
    localStorage.clear();
    this.logFileTxtMetadata = null;
  }

}

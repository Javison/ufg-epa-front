import { TestBed } from '@angular/core/testing';

import { AuthAnonymousGuard } from './auth-anonymous.guard';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { AngularFireAuth } from '@angular/fire/auth';
import { of } from 'rxjs';

describe('AuthAnonymousGuard', () => {
  let guard: AuthAnonymousGuard;

  const authState = {
    displayName: null,
    isAnonymous: true,
    uid: '17WvU2Vj58SnTz8v7EqyYYb0WRc2'
  };

  const mockAngularFireAuth: any = {
    auth: jasmine.createSpyObj('auth', {
      'signInAnonymously': Promise.reject({
        code: 'auth/operation-not-allowed'
      }),
    }),
    authState: of(authState)
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports: [LoggerTestingModule],
      providers: [
        { provide: AngularFireAuth, useValue: mockAngularFireAuth}
      ]
    });
    guard = TestBed.inject(AuthAnonymousGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});

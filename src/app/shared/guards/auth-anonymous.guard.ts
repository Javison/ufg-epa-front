import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
/**
 * Allow access only to anonymous authenticated users
 */
@Injectable({
  providedIn: 'root'
})
export class AuthAnonymousGuard implements CanActivate {

  constructor(public authService: AuthService) {

  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if(this.authService.isLoggedInAny !== true) {
      // this.router.navigate(['/admin/sign-in'])
      console.warn('User is not allow to access');
      return false;
    }

    return true;
  }
  
}

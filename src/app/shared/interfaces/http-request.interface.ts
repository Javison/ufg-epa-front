/**
 * Http Request description
 */
export interface HTTPRequest {
    method: string;
    url: string;
    protocol: string;
    protocol_version: string;
}
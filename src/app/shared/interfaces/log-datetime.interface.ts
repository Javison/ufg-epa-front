/**
 * Log date time from server
 * month: number --> 0 - 11 !!
 */
export interface LogDatetime {
    year: number,
    month: number;
    day: number;
    hour: number;
    minute: number;
    second: number;
    timestamp: string;
}
import { HTTPRequest } from './http-request.interface';
import { LogDatetime } from './log-datetime.interface';

/**
 * Log record from EPA server
 */
export interface LogRecord {
    host: string;
    datetime: LogDatetime;
    request: HTTPRequest;
    response_code: number;
    document_size: number;
    logline: string;
}
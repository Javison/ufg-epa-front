/**
 * Uploaded log file metadata. 
 * Class will be updated by backend to notify user
 */
export class LogFileTxtMetadata {  
    id: string;
    url: string;
    tags: string;
    fileName: string;
    isActive: boolean;
    storagePath: string;
    transformStatus: string;
    urlJsonTransformedFile: string;
  }
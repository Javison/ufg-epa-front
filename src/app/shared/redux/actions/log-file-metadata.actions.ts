


import { Action } from '@ngrx/store'
import { LogFileTxtMetadata } from '../../models/log-file-txt-metadata.model';


export const SET_LOG_FILE_METADATA = '[LogFileMetadata] Set log file metadata';
export const UNSET_LOG_FILE_METADATA = '[LogFileMetadata] Unset log file metadata';
export const UPDATE_LOG_FILE_METADATA = '[LogFileMetadata] Update log file metadata';



export class SetFileMetadataAction implements Action {
  readonly type = SET_LOG_FILE_METADATA;

  constructor( public logfileMetadata: LogFileTxtMetadata ) {}
}

export class UnsetLogFileMetadataAction implements Action {
  readonly type = UNSET_LOG_FILE_METADATA;
}

export class UpdateLogFileMetadataAction implements Action {
  readonly type = UPDATE_LOG_FILE_METADATA;

  constructor( public id: string, public logfileMetadata: LogFileTxtMetadata ) {}
}


export type Actions = SetFileMetadataAction 
                    | UnsetLogFileMetadataAction 
                    | UpdateLogFileMetadataAction;



import { Action } from '@ngrx/store';

export const LOADING_ADD = '[UI] Loading add...';
export const LOADING_REMOVE = '[UI] Loading remove...';
export const TOGGLE_SIDEBAR = '[UI] Toggle sidebar...';

export const LOAD_ITEM_CHARTS = 'charts';
export const LOAD_ITEM_PROCESS_JSON_CHART_DATA = 'process_json_chart_data';
export const LOAD_ITEM_UPLOADING_FILE = 'uploading_file';
export const LOAD_ITEM_LOG_FILE_METADATA = 'log_file_metadata';

export class ToggleSidebarAction implements Action {
  readonly type = TOGGLE_SIDEBAR;
}

export class LoadingAddAction implements Action {
  readonly type = LOADING_ADD;
  constructor( public item: string ) { }
}

export class LoadingRemoveAction implements Action {
  readonly type = LOADING_REMOVE;
  constructor( public item: string ) { }
}

export type acciones = ToggleSidebarAction
                      | LoadingAddAction
                      | LoadingRemoveAction;



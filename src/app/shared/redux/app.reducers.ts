import { ActionReducerMap } from '@ngrx/store';

import * as fromUI from './reducers/ui.reducer';
import * as fromLogFileMetadata from './reducers/log-file-metadata.reducer';


export interface AppState {
  ui: fromUI.UIState;
  logFileMetadata: fromLogFileMetadata.LogFileMetadataState;
}

export const appReducers: ActionReducerMap<AppState> = {
  ui: fromUI.uiReducer,
  logFileMetadata: fromLogFileMetadata.logFileMetadataReducer
}

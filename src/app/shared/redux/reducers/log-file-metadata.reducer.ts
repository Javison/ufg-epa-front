


import * as fromLogFileMetadata from '../actions/log-file-metadata.actions';
import { LogFileTxtMetadata } from '../../models/log-file-txt-metadata.model';

export interface LogFileMetadataState {
  logFileMetadata: LogFileTxtMetadata;
}

const initState: LogFileMetadataState = {
    logFileMetadata: null
};

/**
 * Specify how the application's state changes in response to actions
 * @param state LogFileMetadata status
 * @param action Change requested
 * @returns New LogFileMetadata status
 */
export function logFileMetadataReducer( 
                  state = initState, 
                  action: fromLogFileMetadata.Actions ): LogFileMetadataState {

    switch ( action.type ) {

        case fromLogFileMetadata.SET_LOG_FILE_METADATA:
          
            return {
              logFileMetadata: {
                ...action.logfileMetadata
              }
            };
    
        case fromLogFileMetadata.UPDATE_LOG_FILE_METADATA:

            return {
                ...state
            };

        case fromLogFileMetadata.UNSET_LOG_FILE_METADATA:

            return {
                logFileMetadata: null
            };

        default:
            return state;
    }
}
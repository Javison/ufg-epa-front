


import * as fromUI from '../actions/ui.actions';

export interface UIState {
    showSidebar: boolean;
    loadingItems: string[];
}

const initState: UIState = {
    showSidebar: false,
    loadingItems: []
};
/**
 * Specify how the application's state changes in response to actions
 * @param state User Interface status
 * @param action Change requested
 * @returns New User Interface status
 */
export function uiReducer( state = initState, 
                           action: fromUI.acciones ): UIState {

    switch ( action.type ) {

        case fromUI.LOADING_ADD:
          return {
            ...state,
            loadingItems: [...state.loadingItems, action.item]
          };

        case fromUI.LOADING_REMOVE:
            return {
                ...state,
                loadingItems: [
                  ...state.loadingItems.filter( iItem => iItem !== action.item )
              ]
            };

        case fromUI.TOGGLE_SIDEBAR:
            return {
              ...state,
              showSidebar: !state.showSidebar
            };
    
        default:
            return state;
    }
}
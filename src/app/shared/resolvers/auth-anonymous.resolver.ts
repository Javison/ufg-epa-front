import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { Resolve } from '@angular/router';
import { AuthService } from '../services/auth.service';

/**
 * The interface defines a resolve() method that is invoked when the navigation starts. 
 * The router waits for the data to be resolved before the route is finally activated.
 */
@Injectable()
export class AuthAnonymousService implements Resolve<any> {
 
  constructor(public authService: AuthService) { }

 
  resolve(): Observable<any> {
    if (this.authService.isLoggedInAny) {
      return from(Promise.resolve());
    } else {
      return from(this.authService.signInAnonymously());
    }
  }
 
}
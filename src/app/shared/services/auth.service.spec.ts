import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, of } from 'rxjs';

describe('AuthService', () => {
  let service: AuthService;

  const authState = {
    displayName: null,
    isAnonymous: true,
    uid: '17WvU2Vj58SnTz8v7EqyYYb0WRc2'
  };

  const mockAngularFireAuth: any = {
    auth: jasmine.createSpyObj('auth', {
      'signInAnonymously': Promise.reject({
        code: 'auth/operation-not-allowed'
      }),
    }),
    authState: of(authState)
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ ],
      imports: [LoggerTestingModule],
      providers: [
        { provide: AngularFireAuth, useValue: mockAngularFireAuth }
      ]
    });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});


class SomeService {
  getData(): Observable<string> {
    return 
  }
}
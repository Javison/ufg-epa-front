import { Injectable } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { NGXLogger } from 'ngx-logger';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  fbUserAnonymous: firebase.auth.UserCredential;

  constructor(private log: NGXLogger,
              private afAuth: AngularFireAuth) {

    this.initAuthListener();
  }


  /**
   * 
   */
  initAuthListener() {
    this.log.debug('AuthService > initAuthListener');

    this.afAuth.authState
      .subscribe( (fbUser: firebase.User) => {

        if (fbUser) {
          this.log.debug('AuthService > initAuthListener - fbUser:', fbUser);
          localStorage.setItem('user', JSON.stringify(fbUser));
          
        } else {
          localStorage.setItem('user', null);
        }
      });
  }

  /**
   * Sign in Anonymous
   */
  signInAnonymously(): Promise<any> {

    return this.afAuth.auth.signInAnonymously()
      .then( (fbAuthUserCredential) => {
        this.fbUserAnonymous = fbAuthUserCredential;
      })
      .catch((error) => {
        this.log.error('AuthService > SignInAnonymously: error', error);
      });
  }


  /**
   * Returns true when user is looged in any type (Anonymous included)
   */
  get isLoggedInAny(): boolean {    
    const user = JSON.parse(localStorage.getItem('user'));
    // Desactivado confirmacion de email
    // return (user !== null && user.emailVerified !== false) ? true : false;
    return (user !== null) ? true : false;
  }
}

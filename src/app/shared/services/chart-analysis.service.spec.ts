import { TestBed, async } from '@angular/core/testing';

import { ChartAnalysisService } from './chart-analysis.service';
import { GraphicalAnalysisComponent } from '../../pages/graphical-analysis/graphical-analysis.component';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { AngularFireDatabase } from '@angular/fire/database';
import { Store } from '@ngrx/store';

describe('ChartAnalysisService', () => {


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraphicalAnalysisComponent ],
      imports: [LoggerTestingModule],
      providers: [
        { provide: AngularFireDatabase, useValue: ''},
        { provide: Store, useValue: ''}
      ]
    })
    .compileComponents();
  }));

  it('should be created', () => {
    const service: ChartAnalysisService = TestBed.get(ChartAnalysisService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../redux/app.reducers';
import { LoadingAddAction, LoadingRemoveAction, LOAD_ITEM_CHARTS, LOAD_ITEM_PROCESS_JSON_CHART_DATA } from '../redux/actions/ui.actions';
import { filter, take } from 'rxjs/operators';
import { NGXLogger } from 'ngx-logger';
import { NgfireHelperService } from './ngfire-helper.service';
import { LogRecord } from '../interfaces/log-record.interface';
import HttpStatusCodeUtils from '../utils/http-status-code.utils';

/**
 * Controller to request json read and set group data information that will be displayed
 */
@Injectable({
  providedIn: 'root'
})
export class ChartAnalysisService {
  
  // analytics = firebase.analytics();
  logRecords: LogRecord[] = [];
  httpResponseSizes = new Map<number, number>();
  httpResponseCodes = new Map();
  httpRequestMethods = new Map();
  httpRequestsPerMinute = new Map();

  RequestMethodsPieChartData: any[] = [];

  constructor(private log: NGXLogger,
              private store: Store<AppState>,
              private ngFireService: NgfireHelperService) { }

  /**
   * Retrieve json data from file or server
   */
  async getChartData() {
    this.log.debug('ChartAnalysisService > getChartData');
    this.store.dispatch( new LoadingAddAction(LOAD_ITEM_CHARTS) );

    this.logRecords = await this.ngFireService.getChartData();

    this.processChartData();

    this.store.dispatch( new LoadingRemoveAction(LOAD_ITEM_CHARTS) );
  }

  /**
   * Estructure de data in different groups of information
   */
  processChartData() {    
    this.store.dispatch( new LoadingAddAction(LOAD_ITEM_PROCESS_JSON_CHART_DATA) );

    this.logRecords.forEach( iLogRecord => {
      this.groupHttpRequestMethods(iLogRecord);      
      this.groupHttpResponseCodes(iLogRecord);      
      this.groupHttpRequestsPerMinute(iLogRecord);
      this.groupHttpResponseSizes(iLogRecord, 200, 1000);
    });
    this.httpRequestMethods = this.orderMapByValue(this.httpRequestMethods, false);
    this.httpResponseCodes = this.orderMapByKey(this.httpResponseCodes, false);
    this.httpResponseSizes = this.orderMapByKey(this.httpResponseSizes, false);

    this.store.dispatch( new LoadingRemoveAction(LOAD_ITEM_PROCESS_JSON_CHART_DATA) );
  }
  
  /**
   * 
   * @param iLogRecord 
   */
  groupHttpRequestMethods(iLogRecord: LogRecord) {
    if (iLogRecord.request?.method) {
      this.groupInMap(this.httpRequestMethods, iLogRecord.request.method);
    }
  }

  /**
   * 
   * @param iLogRecord 
   */
  groupHttpResponseCodes(iLogRecord: LogRecord) {
    if (iLogRecord.response_code) {
      const responseCodeAndDescription: string = `${iLogRecord.response_code} (${HttpStatusCodeUtils.getHttpResponseCodeDescription(iLogRecord.response_code)})`;
      this.groupInMap(this.httpResponseCodes, responseCodeAndDescription);
    }
  }

  groupHttpRequestsPerMinute(iLogRecord: LogRecord) {
    if (iLogRecord.datetime?.timestamp) {
      const requestMinute: string = iLogRecord.datetime.timestamp.substr(8, 8).replace('T', ' ');
      this.groupInMap(this.httpRequestsPerMinute, requestMinute);
    }
  }

  groupHttpResponseSizes(iLogRecord: LogRecord, responseCode: number, maxDocumentSize: number) {
    if (iLogRecord.document_size < maxDocumentSize && iLogRecord.response_code === responseCode) {
      // Groups of 10 Bytes
      const responseSizeRound: number = Math.round(iLogRecord.document_size / 10);
      this.groupInMap(this.httpResponseSizes, responseSizeRound);
    }
  }

  orderMapByKey(map: Map<any, any>, reverse: boolean): Map<any, any> {
    if (reverse) {
      return new Map([...map.entries()].reverse());
    }
    return new Map([...Array.from(map.entries())].sort((a, b) => a[0] > b[0] ? 1 : -1));
    // return new Map([...map.entries()].sort());
  }

  orderMapByValue(map: Map<any, any>, reverse: boolean): Map<any, any> {
    if (reverse) {
      return new Map([...map.entries()].sort((a, b) => a[1] - b[1]));
    }
    return new Map([...map.entries()].sort((a, b) => b[1] - a[1]));
  }

  groupInMap(map: Map<any, any>, key: any) {
    if (key) {        
      if ( map.get(key) ) {
        map.set(key, map.get(key) + 1);
        
      } else {
        map.set(key, 1);      
      }
    }
  }

}

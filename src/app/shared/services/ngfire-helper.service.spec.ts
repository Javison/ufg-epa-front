import { TestBed } from '@angular/core/testing';

import { NgfireHelperService } from './ngfire-helper.service';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { AngularFireDatabase } from '@angular/fire/database';

describe('NgfireHelperService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({

      providers: [
        { provide: AngularFireDatabase, useValue: ''}
      ],
      imports: [LoggerTestingModule],      
    })
  });

  it('should be created', () => {
    const service: NgfireHelperService = TestBed.get(NgfireHelperService);
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import { NGXLogger } from 'ngx-logger';
import { AngularFireDatabase } from '@angular/fire/database';
import { take } from 'rxjs/operators';
import { LogFileTxtMetadata } from '../models/log-file-txt-metadata.model';
import { LogRecord } from '../interfaces/log-record.interface';
import { Observable } from 'rxjs';
import epalog from '../../../assets/epa-log/epa-http.json';

@Injectable({
  providedIn: 'root'
})
export class NgfireHelperService {
  
  readonly PATH_APP_SERVER_ANALYSIS: string = "app/server_analysis";
  readonly PATH_ETL: string = "etl";
  readonly PATH_DATA: string = "data";
  readonly PATH_SOURCE: string = "source";
  readonly PATH_UPLOADS: string = "uploads";
  readonly PATH_STAGING: string = "staging";
  readonly PATH_WAREHOUSE: string = "warehouse";

  constructor(private log: NGXLogger,
              private db: AngularFireDatabase) { }

  /**
   * @returns promise with array of log records
   */
  getChartData(): Promise<LogRecord[]> {
    this.log.debug('NgfireHelperService > getChartData');
    return epalog as unknown as Promise<LogRecord[]>;
    // NOTE 1.- Assignment states "read the JSON-File". Nevertheless I would rather read from DB. Comment previous and uncomment next 2:
    // const path = `${this.PATH_APP_SERVER_ANALYSIS}/${this.PATH_ETL}/${this.PATH_STAGING}/${this.PATH_DATA}`
    // return this.db.list(path, ref => ref.orderByKey()).valueChanges().pipe(take(1)).toPromise() as Promise<LogRecord[]>;    
    // NOTE 2.- When read json from DB, might be useful to limit results when testing. ie.: 400
    // return this.db.list(path, ref => ref.limitToFirst(400).orderByKey()).valueChanges().pipe(take(1)).toPromise() as Promise<LogRecord[]>;    
  }

  /**
   * Save on database txt file metadata
   * 
   * @param logFileTxt txt file metadata from uploaded file
   */
  pushLogFileMetadata(logFileTxt: LogFileTxtMetadata): Promise<void> {
    const path = `${this.PATH_APP_SERVER_ANALYSIS}/${this.PATH_ETL}/${this.PATH_SOURCE}/${this.PATH_DATA}/${this.PATH_UPLOADS}`
    logFileTxt.id = this.db.createPushId();
    return this.db.list(path).set(logFileTxt.id, logFileTxt);
  }

  /**
   * Observe txt metadata file information
   * 
   * @param logFileTxtMetadataId Uploaded txt file identifier in database
   * @returns Observer Logfile metadata information
   */
  subscribeBackendFileTransformation(logFileTxtMetadataId: string): Observable<any> {
    const path = `${this.PATH_APP_SERVER_ANALYSIS}/${this.PATH_ETL}/${this.PATH_SOURCE}/${this.PATH_DATA}/${this.PATH_UPLOADS}/${logFileTxtMetadataId}`
    return this.db.object(path).valueChanges();
  }

}

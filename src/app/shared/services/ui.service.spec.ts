import { TestBed } from '@angular/core/testing';

import { UIService } from './ui.service';
import { ToastPackage, ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('UIService', () => {

  beforeEach(() => {
    
    TestBed.configureTestingModule({
      imports: [
        ToastrModule.forRoot(),
        BrowserAnimationsModule 
      ],
      declarations: [],
      providers: [
        { provide: ToastPackage, useValue: '' }
      ],
  })
  });

  it('should be created', () => {
    const service: UIService = TestBed.get(UIService);
    expect(service).toBeTruthy();
  });

});

import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
/**
 * Service class to centralize UI actions such as side bar open / close and notifications
 */
@Injectable({
  providedIn: 'root'
})
export class UIService {

  constructor(private toastr: ToastrService) { }

  showSuccess(message: string) {
    this.toastr.success(message);
  }

  showError(message: string) {
    this.toastr.error(message, 'Upsss');
  }

  showWarning(message: string) {
    this.toastr.warning(message);
  }
}

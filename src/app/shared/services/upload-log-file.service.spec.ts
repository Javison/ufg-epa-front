import { TestBed } from '@angular/core/testing';

import { UploadLogFileService } from './upload-log-file.service';
import { LoggerTestingModule } from 'ngx-logger/testing';
import { AngularFireDatabase } from '@angular/fire/database';
import { Store } from '@ngrx/store';
import { AngularFireStorage } from '@angular/fire/storage';
import { ToastrModule } from 'ngx-toastr';

describe('UploadLogFileService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: AngularFireDatabase, useValue: ''},
        { provide: AngularFireStorage, useValue: '' },
        { provide: Store, useValue: ''},
       ],
      imports: [
        LoggerTestingModule,
        ToastrModule.forRoot()
      ]
    })
  });

  it('should be created', () => {
    const service: UploadLogFileService = TestBed.get(UploadLogFileService);
    expect(service).toBeTruthy();
  });
});

import { Injectable, EventEmitter } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable, Subscription } from 'rxjs';
import { NGXLogger } from 'ngx-logger';
import { NgfireHelperService } from './ngfire-helper.service';
import { LogFileTxtMetadata } from '../models/log-file-txt-metadata.model';
import { finalize } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../redux/app.reducers';
import { LoadingAddAction, LOAD_ITEM_UPLOADING_FILE, LoadingRemoveAction, LOAD_ITEM_LOG_FILE_METADATA } from '../redux/actions/ui.actions';
import { UIService } from './ui.service';
import { SetFileMetadataAction } from '../redux/actions/log-file-metadata.actions';

@Injectable({
  providedIn: 'root'
})
export class UploadLogFileService {


  downloadURL: Observable<string>;  
  uploadProgress$ = new EventEmitter<number>();  
  logFileTxtMetadataSubscription: Subscription = new Subscription();
    
  constructor(private log: NGXLogger,
              private store: Store<AppState>,
              private uiService: UIService,
              private afStorage: AngularFireStorage,
              private ngFireService: NgfireHelperService) { }

  /**
   * Cancell subscriptions of this service responsability 
   */
  cancellSubscriptions() {
    this.log.debug('UploadLogFileService > cancellSubscriptions');
    this.logFileTxtMetadataSubscription.unsubscribe();
  }  

  /**
   * 
   * @param file 
   * @param PREFIX_UPLOADED_FILE 
   * @param STORAGE_PATH 
   */
  uploadFile(file: any, PREFIX_UPLOADED_FILE: string, STORAGE_PATH: string) {
    this.log.debug('UploadLogFileService > uploadFile - file: ', file);

    let logFileTxtMetadata: LogFileTxtMetadata = new LogFileTxtMetadata();
    // TODO add and save more file metadata

    // Files with same name will be overwritten. We generate "unique" file id with random (0 - 99999)
    let random = [...Array(5)].map(i=>(~~(Math.random()*10)).toString(10)).join('');

    logFileTxtMetadata.fileName = PREFIX_UPLOADED_FILE + random;
    logFileTxtMetadata.storagePath = STORAGE_PATH + logFileTxtMetadata.fileName;
    this.log.debug('UploadLogFileComponent > onClickUploadFile - fileName & path', logFileTxtMetadata.fileName, logFileTxtMetadata.storagePath);

    this.uploadTaskGoogleCloud(file, logFileTxtMetadata); 
  }

  /**
   * 
   * @param file 
   * @param logFileTxtMetadata 
   */
  uploadTaskGoogleCloud(file: string, logFileTxtMetadata: LogFileTxtMetadata) {
    this.store.dispatch( new LoadingAddAction(LOAD_ITEM_UPLOADING_FILE) );

    const ref = this.afStorage.ref(logFileTxtMetadata.storagePath);
    const task = ref.put(file);
    
    task.catch( (err) => {
      this.log.error('UploadLogFileService > uploadTaskGoogleCloud err', err);
      this.uiService.showError('Couldnt upload file. Please confirm the file is correct and try again later');
      this.store.dispatch( new LoadingRemoveAction(LOAD_ITEM_UPLOADING_FILE) );
    });

    task.percentageChanges().subscribe( (progress) => {
      this.uploadProgress$.emit(progress);
    });

    task.snapshotChanges().pipe(
      finalize( () => {
        this.downloadURL = ref.getDownloadURL()
        this.downloadURL.subscribe(
          url => {
            this.log.debug('UploadLogFileService > uploadTaskGoogleCloud - url', url);
            logFileTxtMetadata.url = url;
            logFileTxtMetadata.transformStatus = 'uploaded';
            this.pushUploadedFileMetadata(logFileTxtMetadata);
            this.store.dispatch( new LoadingRemoveAction(LOAD_ITEM_UPLOADING_FILE) );
          });
      })
    ).subscribe();

  }

  /**
   * 
   * @param logFileTxtMetadata 
   */
  pushUploadedFileMetadata(logFileTxtMetadata: LogFileTxtMetadata) {
    this.log.debug('UploadLogFileService > pushUploadedFileMetadata');

    this.ngFireService.pushLogFileMetadata(logFileTxtMetadata)
      .then((_) => {
        this.log.debug('UploadLogFileService > pushUploadedFileMetadata Ok');
        this.uiService.showSuccess('File uploaded successfully');
        localStorage.setItem('logFileTxtMetadataId', logFileTxtMetadata.id);
        this.subscribeLogFileMetadata(logFileTxtMetadata.id);
      })
      .catch((err) => {
        this.log.error('UploadLogFileService > pushUploadedFileMetadata err', err);
        this.uiService.showError('Couldnt upload file. Please try again later');
    });
  }


  /**
   * Retrieve info about uploaded file. This information will be updated by backend server.
   * @param logFileTxtMetadataId 
   */
  subscribeLogFileMetadata(logFileTxtMetadataId: string) {
    this.log.debug('UploadLogFileService > subscribeLogFileMetadata - logFileTxtMetadataId: ', logFileTxtMetadataId);
    
    this.logFileTxtMetadataSubscription = 
      this.ngFireService.subscribeBackendFileTransformation(logFileTxtMetadataId)
        .subscribe(
          (logFileTxtMetadata: LogFileTxtMetadata) => {
            this.log.debug('UploadLogFileService > subscribeLogFileMetadata - logFileTxtMetadata', logFileTxtMetadata);
            this.store.dispatch( new SetFileMetadataAction(logFileTxtMetadata) );
          },
          (err) => {
            this.log.debug('UploadLogFileService > subscribeLogFileMetadata err', err);
          }
      );    
  }

}

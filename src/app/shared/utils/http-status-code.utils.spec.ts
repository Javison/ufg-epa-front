import HttpStatusCodeUtils from './http-status-code.utils';


describe('HttpStatusCodeUtils', () => {
  
  it('should return correct description for Http response 100', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(100))
        .toEqual('Continue');
  });

  it('should return correct description for Http response 200', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(200))
        .toEqual('OK');
  });

  it('should return correct description for Http response 300', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(300))
        .toEqual('Multiple Choices');
  });

  it('should return correct description for Http response 400', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(400))
        .toEqual('Bad Request');
  });

  it('should return correct description for Http response 500', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(500))
        .toEqual('Internal Server Error');
  });

  it('should return unknown description for Http response 0', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(0))
        .toEqual('Unknown');
  });

  it('should return unknown description and log error for Http response that not exist 550', () => {
    expect(HttpStatusCodeUtils.getHttpResponseCodeDescription(550))
        .toEqual('Unknown');
  });

});

/**
 * Util class to get description from a numeric Http Response code.
 * This codes can be found at:
 * - assets/docs/http-response-codes.pdf
 * - https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
 */
export default class HttpStatusCodeUtils {
            
    /**
     * Main function to be called
     * @param code Http response code
     * @returns Http response code description 
     */
    static getHttpResponseCodeDescription(code: number): string {

        try {
            let httpStatusGroup: string = code.toString();
            if (httpStatusGroup.startsWith('1')) {
                return HttpStatusCodeUtils.getInProgressCodeDescription(code);
            } else if (httpStatusGroup.startsWith('2')) {
                return HttpStatusCodeUtils.getSuccessfulCodeDescription(code);
            } else if (httpStatusGroup.startsWith('3')) {
                return HttpStatusCodeUtils.getRedirectionCodeDescription(code);
            } else if (httpStatusGroup.startsWith('4')) {
                return HttpStatusCodeUtils.getClientErrorCodeDescription(code);
            } else if (httpStatusGroup.startsWith('5')) {
                return HttpStatusCodeUtils.getServerErrorCodeDescription(code);
            }
            
        } catch (error) {
            console.warn('HttpStatusCodeUtils > getHttpResponseCodeDescription - error: ', error);
        }
        
        return 'Unknown'
    }

    /**
     * Description for 1xx group
     * @param code Http response code
     * @returns Http response code description 
     */
    static getInProgressCodeDescription(code: number): string {
        switch (code) {
            case 100:
                return 'Continue';        
            case 101:
                return 'Switching Protocols';  
            default:
                throw new Error(`Unknown In progress http code: ${code}`);
        }
    }

    /**
     * Description for 2xx group
     * @param code Http response code
     * @returns Http response code description 
     */
    static getSuccessfulCodeDescription(code: number): string {
        switch (code) { 
            case 200:
                return 'OK';        
            case 202:
                return 'Accepted';        
            case 203:
                return 'Non-Authoritative Information';        
            case 204:
                return 'No Content'; 
            case 206:
                return 'Partial Content';     
            default:
                throw new Error(`Unknown Successful http code: ${code}`);
        }
    }

    /**
     * Description for 3xx group
     * @param code Http response code
     * @returns Http response code description 
     */
    static getRedirectionCodeDescription(code: number): string {
        switch (code) {
            case 300:
                return 'Multiple Choices';        
            case 301:
                return 'Moved Permanently';   
            case 302:
                return 'Found';        
            case 303:
                return 'Accepted';        
            case 304:
                return 'See Other';        
            case 305:
                return 'Use Proxy'; 
            case 306:
                return 'Unused';     
            case 307:
                return 'TemporaryRedirect';     
            default:
                throw new Error(`Unknown Redirection http code: ${code}`);
        }
    }

    /**
     * Description for 4xx group
     * @param code Http response code
     * @returns Http response code description 
     */
    static getClientErrorCodeDescription(code: number): string {
        switch (code) {
            case 400:
                return 'Bad Request';        
            case 401:
                return 'Unauthorized';   
            case 402:
                return 'Payment Required';        
            case 403:
                return 'Forbidden';        
            case 404:
                return 'Not Found';        
            case 405:
                return 'Method Not Allowed'; 
            case 406:
                return 'Not Acceptable';     
            case 407:
                return 'ProxyAuthenticationRequired';     
            case 408:
                return 'Request Timeout';     
            case 409:
                return 'Conflict';     
            case 410:
                return 'Gone';     
            case 411:
                return 'Lengh Required';     
            case 412:
                return 'Precondition Failed';     
            case 413:
                return 'ReqeustEntityTooLarge';    
            case 415:
                return 'Unsupported Media Type';     
            case 416:
                return 'Request Range Not Satisfiable';     
            case 417:
                return 'Expectation Failed';     
            default:
                throw new Error(`Unknown Client error http code: ${code}`);
        }
    }

    /**
     * Description for 5xx group
     * @param code Http response code
     * @returns Http response code description 
     */
    static getServerErrorCodeDescription(code: number): string {
        switch (code) {
            case 500:
                return 'Internal Server Error';        
            case 501:
                return 'Not Implemented';   
            case 502:
                return 'Bad Gateway';        
            case 503:
                return 'Service Unavailable';        
            case 504:
                return 'GatewayTimeout';        
            case 505:
                return 'HTTP Version Not Supported'; 
            default:
                throw new Error(`Unknown Server error http code: ${code}`);
        }
    }


}
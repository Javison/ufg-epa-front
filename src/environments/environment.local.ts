/**
 * This file can be replaced during build by using the `fileReplacements` array.
 * `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
 * The list of file replacements can be found in `angular.json`.
 */
export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyAkbrT0fr34NdiSWzjGqjA9nbiaa5KYKhQ",
    authDomain: "http://localhost",
    databaseURL: "http://localhost:9000/?ns=ufg-epa",
    projectId: "ufg-epa",
    storageBucket: "ufg-epa.appspot.com",
    messagingSenderId: "123456789",
    appId: "1:123456789:web:123456789abcdef",
    measurementId: "G-ABCD123456789"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

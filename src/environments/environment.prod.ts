/**
 * This file can be replaced during build by using the `fileReplacements` array.
 * `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
 * The list of file replacements can be found in `angular.json`.
 */
export const environment = {
  production: true,

  firebase: {
    apiKey: "AIzaSyAkbrT0fr34NdiSWzjGqjA9nbiaa5KYKhQ",
    authDomain: "ufg-epa.firebaseapp.com",
    databaseURL: "https://ufg-epa.firebaseio.com",
    projectId: "ufg-epa",
    storageBucket: "ufg-epa.appspot.com",
    messagingSenderId: "258286891714",
    appId: "1:258286891714:web:1882fed6ce07458820415b",
    measurementId: "G-XQD77EY9RJ"
  }
};

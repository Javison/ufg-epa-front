/**
 * This file can be replaced during build by using the `fileReplacements` array.
 * `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
 * The list of file replacements can be found in `angular.json`.
 */
export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyAkbrT0fr34NdiSWzjGqjA9nbiaa5KYKhQ",
    authDomain: "ufg-epa.firebaseapp.com",
    databaseURL: "https://ufg-epa.firebaseio.com",
    projectId: "ufg-epa",
    storageBucket: "ufg-epa.appspot.com",
    messagingSenderId: "258286891714",
    appId: "1:258286891714:web:1882fed6ce07458820415b",
    measurementId: "G-XQD77EY9RJ"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
